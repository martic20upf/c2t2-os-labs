//
// Created by martic20 on 23/01/20.
//
#include "code/myutils.c"

int main (int argc, char* argv[]){
    startTimer(0); //for showing the execution time
    
    char buff[80]; //string to store
    char end_char = ' '; //last char readed
    int bytes_readed = -1; //nº of chars readed
    int result = 0; //result from the sum
    
    //sum all inputs from arguments
    for(int i=1; i < argc; i++){
        int aux = 0; //int to store the cast from string
        sscanf(argv[i], "%d", &aux);
        result += aux;
    }

    //sum all inputs from stdin
    while(bytes_readed != 0) {
        bytes_readed = read_split(0, buff, sizeof(buff), &end_char);
        int aux = 0; //int to store the cast from string
        sscanf(buff, "%d", &aux);
        result += aux;
    }

    printf("%d \n", result); //print final sum result
    
    long time = endTimer(0);
    printf("Time: %ld ms\n", time); //show total execution time
    return 0;
}