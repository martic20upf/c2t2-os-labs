#include <stdio.h>
#include "code/myutils.c"
#include <stdbool.h>
#define FILE "code/cmds.txt"

int main(){
    startTimer(1); //for showing the execution time

	char ch_end;
	char buff[100];// declarem un buffer
	int num_chars = -1;//utilitzem aquesta varaiable per entrar al while
	int cont = 0;

	int fd = open(FILE,O_RDWR);//obrim el fitxer

	while (num_chars != 0) {
        num_chars = read_split(fd, buff, sizeof(buff), &ch_end);//amb el read and split llegirem cada paraula, ens retornara el número de caràcters que haurà llegit.

        if (buff[0] == '&') {//comprovem si la primera posició del buffer es "&"
            printf("Launched in the Background\n");
        } else if (num_chars!=0){//comprovem que el número de caràcters que ha llegit és diferent de 0
            bool is_background = false;//creem una variable boleana
            char* ampersand = strchr(buff, '&');
            if (NULL != ampersand) {
                is_background = true;
                *ampersand = '\0';
            }
            if (cont == 0){//si és la primera paraula
                printf("Command Name: %s \n", buff);
                cont ++;
            } else {//si la paraula és després de la primera i no és un "&"
                printf("Param %d: %s\n",cont, buff);
                cont ++;
            }
            if(is_background){//mirem si hi ha "&"
                printf("Launched in the Background\n");
            }
        }
        if(ch_end == '\n') cont=0;
    }

    long time = endTimer(1);
    printf("\nTime: %ld ms\n", time); //show total execution time
    return 0;
}
