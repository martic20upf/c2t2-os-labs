#ifndef MYUTILS
#define MYUTILS

#include <sys/time.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <unistd.h>     // unix-like system calls read and write
#include <fcntl.h>      // unix-like file handling : open
#include <stdlib.h>     // standard C lib input output basic functions compatible with Windows
#include <stdio.h>      // standard C lib input output basic functions compatible with Windows
#include <string.h>     // also from standard C lib : basic string functions like strlen


typedef struct country_struct {
	char name[30];
	long population;
	int area;	
	int coastline;
} country;

void startTimer(int i);   // start timer i
long endTimer(int i);     // returns millis since timer i started

int read_split( int fin, char* buff, int maxlen, char* ch_end );

// builds an address given the ip and port both as a string (char*)
void fill_addr(char *ip, char* port, struct sockaddr_in* addr);

void printCountry(country c);

// File Lock Region functions : it returns if it is occupied
int fileLockReg(int fd, int start, off_t len);
int fileUnLockReg(int fd, int start, off_t len);

#endif