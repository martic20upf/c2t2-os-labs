#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

int main(void) {
    startTimer(0);
    int     num;
    int     ret;
    int     fd[2];
    int     fd_back[2];

    pipe(fd);           // pipe creation before the fork
    pipe(fd_back);      // pipe comunicació de fill a pare
    
    ret = fork();

    if(ret == 0) {     // Child code: must end with an exit
        close(fd[1]);

        for(int i=0;i<1000;i++){
            read(fd[0], &num, sizeof(int));       // reading in the pipe fd[0]  
            printf("Child Received: %d\n", num);

            num = num+1;
            write(fd_back[1], &num, sizeof(num));  // Fill escriu al pare
            printf("Child Sent: %d\n", num);  

            
        }
        exit(0);
    }

    // Father code ONLY: because we have an exit in the child code
    close(fd[0]);
    num = 0; // ho canviem a 0 per tal de poder fer 1000 iteracions i arribar al num 1000

    for(int i=0;i<1000;i++){
        printf("Father Sent: %d\n", num);
        write(fd[1], &num, sizeof(int));          // writting in the pipe fd[0]

        read(fd_back[0], &num, sizeof(int));       // el pare llegeix la respota del fill
        printf("Father Received: %d\n", num);
    }

    long time = endTimer(0);
    printf("\nTime: %ld ms\n", time);
    return 0;
}