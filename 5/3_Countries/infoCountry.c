#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "myutils.h"

country c;


int main(int argc, char* argv[])
{
	char buffin[100];
    char buffout[100];

	int fin=open("countries.dat",  O_RDWR, 0600);

	int i = atoi(argv[1]);
	printf("size of country: %lu\n",sizeof(country));

	// add your code
	lseek(fin, i*sizeof(country),SEEK_SET); 
	// Com si el fitxer fos un array i fin el seu punter
	// necessitem moure el punter de dins el fitxer (amb lseek)
	// fins a la posició del país que volem llegir

	read(fin,&c,sizeof(country));
	printCountry(c);

	close(fin);
}