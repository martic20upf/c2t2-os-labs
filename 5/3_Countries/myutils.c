#include "myutils.h"

struct timeval start[100];

void startTimer(int i) {
    if(i>=100) printf("Warning only 100 timers available!!");
    gettimeofday(&start[i], NULL);
}

long endTimer(int i) {
    long mtime, seconds, useconds;    
    struct timeval end;

    gettimeofday(&end, NULL);
    seconds  = end.tv_sec  - start[i].tv_sec;
    useconds = end.tv_usec - start[i].tv_usec;
    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    return mtime;
}

int read_split( int fin, char* buff, int maxlen, char* ch_end ) {
    int i = 0;
    int oneread = 1;
    char c = '.';
    while(c != ' ' && c != '\n' && oneread == 1 && i < maxlen) {
        oneread = read( fin, &c, 1);
        if(c != ' ' && c != '\n' && oneread == 1) {
            buff[i] = c;
            i++;
        }
    }
    *ch_end = c;
    if(i < maxlen) buff[i] = '\0';
    return i;
}



int fileLockReg(int fd, int start, off_t len)
{
    struct flock fl;
    fl.l_type = F_WRLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = start;
    fl.l_len = len;
    return fcntl(fd, F_SETLK, &fl);
}

int fileUnLockReg(int fd, int start, off_t len)
{
    struct flock fl;
    fl.l_type = F_UNLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = start;
    fl.l_len = len;
    return fcntl(fd, F_SETLK, &fl);
}





void fill_addr(char *ip, char* port, struct sockaddr_in* addr) {
    int portnum;
    struct hostent *server = NULL;

    if(ip != NULL) {
        server = gethostbyname(ip);
        if (server == NULL) { fprintf(stderr,"ERROR, no such host\n"); exit(0); }
    }

    memset((char *)addr, 0, sizeof(struct sockaddr_in));
    addr->sin_family = AF_INET;
    
    if(ip == NULL) addr->sin_addr.s_addr = INADDR_ANY;
    else { 
        memcpy((char *)&addr->sin_addr.s_addr, 
               (char *)server->h_addr, 
               server->h_length);
    }

    portnum = atoi(port);
    addr->sin_port = htons(portnum); // convert port num to network byte order
}



void printCountry(country c) {
    printf("----------------------\n");
    printf("Country: %s\n",c.name);
    printf("  populat: %ld\n",c.population);
    printf("     area: %d\n",c.area);
    printf("    coast: %d\n",c.coastline);
}
