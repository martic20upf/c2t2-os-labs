#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "myutils.h"

country c;

#define NUM_THREADS 5   // number of producer consumer threads

pthread_t threadsIds[NUM_THREADS];

typedef struct thread_params {
	int pop;
	int fid;
	int country_id;
} thread_params;

void* th_func(void *param)
{
	thread_params tp = *((thread_params *) param);
	country readed_country;
	readed_country.population = 0;
	printf(" C ID %d", tp.country_id);
	
	while(1){
		fileLockReg(tp.fid, tp.country_id*sizeof(country), sizeof(country));
		read(tp.fid,&readed_country,sizeof(country));
		lseek(tp.fid, tp.country_id*sizeof(country),SEEK_SET);
		printf(" therdfgd 2134  d\n");
		if(readed_country.population >= (readed_country.population + tp.pop)){
			fileUnLockReg(tp.fid, tp.country_id*sizeof(country), sizeof(country));
			pthread_exit(0);
		}
		readed_country.population++;
		printf(" SUMAMA  d\n");
		write(tp.fid, &readed_country.population, sizeof(readed_country.population)); // escribim les dades actualizades del país al fitxer
		lseek(tp.fid, tp.country_id*sizeof(country),SEEK_SET); 
		fileUnLockReg(tp.fid, tp.country_id*sizeof(country), sizeof(country));
	}
	
}

int main(int argc, char* argv[])
{
	startTimer(0);
	char buffin[100];
    char buffout[100];

    if(argc < 4) {
    	printf("Usage ./sumCountry i_country countries.dat population\n");
    	return 0;
    }

	int fin=open("countries.dat",  O_RDWR, 0600);

	int id = atoi(argv[1]);
	int pop = atoi(argv[3]);

	printf("size of country: %lu\n",sizeof(country));

	// add your code
	lseek(fin, id*sizeof(country),SEEK_SET); 
	// Com si el fitxer fos un array i fin el seu punter
	// necessitem moure el punter de dins el fitxer (amb lseek)
	// fins a la posició del país que volem llegir

	read(fin,&c,sizeof(country));
	printCountry(c);

	lseek(fin, id*sizeof(country),SEEK_SET); //el read altera el offset 
	//per tant necessitem tornar-lo a inicialitzar
	
	thread_params tp;
	tp.pop = pop;
	tp.fid = fin;
	tp.country_id = id;

	for(int i=0;i<NUM_THREADS;i++) {
        pthread_create(&threadsIds[1+i], NULL, th_func, &tp); 
    }

	for(int i=0;i<NUM_THREADS;i++) {
        pthread_join(threadsIds[i], NULL);
    }

	lseek(fin, id*sizeof(country),SEEK_SET);  //el read altera el offset 
	//per tant necessitem tornar-lo a inicialitzar

	//tornem a llegir del fitxer i mostrem la seva informació real actualitzada
	read(fin,&c,sizeof(country));
	printCountry(c);

	close(fin);

	long time = endTimer(0);
    printf("\nTime: %ld ms\n", time);
}