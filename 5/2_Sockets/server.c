/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "myutils.h"

struct sockaddr_in serv_addr, cli_addr;


int main(int argc, char *argv[])
{
     int sockfd, newsockfd;
     socklen_t clilen;
     char buffer[256];
     int n;

     if (argc < 2) { fprintf(stderr,"usage: ./server port\n"); exit(1); }

     sockfd = socket(AF_INET, SOCK_STREAM, 0);     

     printf("socket created\n");
     
     fill_addr(NULL, argv[1], &serv_addr);

     printf("about to bind\n");
     
     bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

     listen(sockfd,10);  // socket file descriptor and number of connections that can be awaiting

     clilen = sizeof(cli_addr);

     printf("listen done\n");

     newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr,  &clilen); // blocks until client connects

     printf("accepted\n");

     int num;

     for(int i=0;i<1000;i++){
          read(newsockfd,&num,sizeof(int)); //rebem un número
          printf("Server received: %d\n",num);

          num++; // incrementem per retorna-li al client

          sprintf(buffer,"Message has been sent");
          write(newsockfd,&num,sizeof(num)); //retornem el valor actualitzat al client
     }

     close(newsockfd);
     close(sockfd);

     return 0; 
}
