#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "myutils.h"


#define N 256

int sockfd;
struct sockaddr_in serv_addr;


int main(int argc, char *argv[])
{
    startTimer(0);
    int n;
    char buffer[N];
    if (argc < 3) { fprintf(stderr,"usage %s hostname port\n", argv[0]); exit(0); }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);       // Client step 1) call to Socket
    
    printf("socket created\n");

    fill_addr(argv[1], argv[2], &serv_addr);

    printf("about to connect\n");

    int retcon = connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)); 

    printf("Please enter a starting number: \n");

    read(0,buffer,N);
    sprintf(buffer,"%s\n",buffer);  // when sending to a web this second '\n' is important

    int num = atoi(buffer);    
    printf("Client sending %d\n",num);
    write(sockfd,&num,sizeof(int));  
    
    // Deal with server response here
    for(int i=0;i<1000;i++){ 
        read(sockfd,&num,sizeof(int)); // obtenim la respota del server amb les dades actualitzades
        printf("Client received: %d\n",num);
        write(sockfd,&num,sizeof(int)); // tornem a enviar-l'hi
    }

    close(sockfd);

    long time = endTimer(0);
    printf("\nTime: %ld ms\n", time);

    return 0;
}
