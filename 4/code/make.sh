function help () {
	echo "Help: "
	echo "Use ./make.sh mon	for monitor version"
	echo "Use ./make.sh sem	for semaphore version"
	echo "Use ./make.sh chr	for execute in reading by char mode"
	echo "Use ./make.sh man	for execute with only user input"
}
if [ $# = 0 ] ; then
	help
elif [ $1 = "-help" ] ; then
	help
elif [ $1 = "sem" ];then
	gcc myutils.c spell.c -lpthread -o spell && ./spell < words.txt
elif [ $1 = "mon" ];then
	gcc myutils.c spell_monitors.c -lpthread -o spell && ./spell < words.txt
elif [ $1 = "chr" ];then
	gcc myutils.c spell.c -lpthread -o spell && ./spell -arg
elif [ $1 = "man" ];then
        gcc myutils.c spell.c -lpthread -o spell && ./spell
else
	echo "Unknown argument"
fi
