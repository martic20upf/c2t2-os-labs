#include <time.h>       // for usleep : waits for microseconds
#include <unistd.h>     // unix-like system calls read and write
#include <fcntl.h>      // unix-like file handling : open
#include <stdio.h>      // standard C lib input output basic functions compatible with Windows
#include <string.h>     // also from standard C lib : basic string functions like strlen
#include <stdbool.h> 
#include <pthread.h>
#include <ctype.h>

#include "myutils.h"

#define NUM_THREADS 2   // number of producer consumer threads
#define N 10            // number of elements in the shared buffer
#define TIMER 1         // timer id used

pthread_t threadsIds[NUM_THREADS];

int in = 0;
int out = 0;
char buffer[N][80];
bool readerFinished = false;
semaphore full; //definim un semaphore full
semaphore empty;//definim un semaphore empty

void* th_read(void *param)
{
    char buff[80];
    char ch_end;
    int nread = 1;
    printf("Hello from thread READ (producer) *******************\n");
    while(nread > 0) {
        nread = read_split(0, buff, 80, &ch_end);     // produceItem
        if(nread > 0) {
            sem_wait(&empty);//wait semaphore empty
            //printf("read_split returned %s length %d\n", buff, nread);
            //while( (in+1) % N == out);     // add item
            strcpy(buffer[in],buff);
            in = (in+1)%N;
            sem_signal(&full);//signal semaphore full
        } else {
            printf("read_split EMPTY length %d\n", nread);
        }
    }
    printf("Thread READ Finished reading **********************\n");

    pthread_exit(0);
}

void* th_check(void *param)
{
    char buff[80];
    printf("Hello from a thread CHECK (consumer)\n");

    while(!readerFinished) {
        sem_wait(&full);//wait semaphore full
        //while(in==out);
        strcpy(buff,buffer[out]);     // take item
        int temp = out;
        out = (out+1)%N;
        sem_signal(&empty);//signal semaphore empty

        //ignore upper cases
        for(int l=0; l< strlen(buff);l++){
            buff[l] = tolower(buff[l]);
        }

        if (search_dict(&buff[0])) {
            printf("thread CHECK looking: %s CORRECT\n",buff);
        }else{
            printf("thread CHECK looking: %s INCORRECT\n",buff);
        }
    }
    pthread_exit(0);
}


int main(int argc, char *argv[])
{
    startTimer(TIMER);
    int i;

    if(argc > 1)  set_stdin_mode(); // Terminal Mode: characters read one by one

    sem_init(&empty, N); //inicialitzem el semaphore empty a N
    sem_init(&full, 0); //inicialitzem el semaphore full a 0

    load_dict(10);

    pthread_create(&threadsIds[0], NULL, th_read, NULL); 
    for(i=0;i<NUM_THREADS-1;i++) {
        pthread_create(&threadsIds[1+i], NULL, th_check, NULL); 
    }

    pthread_join(threadsIds[0], NULL);   // Waiting for the Reader
    readerFinished = true;

    printf("Master Thread Waiting for Checkers\n");   // Waiting for Checkers
    for(i=1;i<NUM_THREADS;i++) {
        sem_signal(&full); // signal semaphore full
    }
    for(i=1;i<NUM_THREADS;i++) {
        pthread_join(threadsIds[i], NULL);
    }
    printf("Master Thread Done\n");

    if(argc > 1) reset_stdin_mode();

    long time = endTimer(TIMER);
    printf("\nTime: %ld ms\n", time); //show total execution time

    return 0;
}

