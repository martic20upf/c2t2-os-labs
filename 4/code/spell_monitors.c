#include <time.h>       // for usleep : waits for microseconds
#include <unistd.h>     // unix-like system calls read and write
#include <fcntl.h>      // unix-like file handling : open
#include <stdio.h>      // standard C lib input output basic functions compatible with Windows
#include <string.h>     // also from standard C lib : basic string functions like strlen
#include <stdbool.h> 
#include <pthread.h>
#include <ctype.h>

#include "myutils.h"

#define NUM_THREADS 2   // number of producer consumer threads
#define N 10            // number of elements in the shared buffer   
#define TIMER 1         // timer id used

pthread_t threadsIds[NUM_THREADS];

int in = 0;
int out = 0;
char buffer[N][80];
bool readerFinished = false;
pthread_mutex_t lock;//declarem un lock
pthread_cond_t empty;//definim un empty
pthread_cond_t full;//definim un full

void* th_read(void *param)
{
    char buff[80];
    char ch_end;
    int nread = 1;
    printf("Hello from thread READ (producer) *******************\n");
    while(nread > 0) {
        nread = read_split(0, buff, 80, &ch_end);     // produceItem
        if(nread > 0) {
            pthread_mutex_lock(&lock);//fem lock
            //printf("read_split returned %s length %d\n", buff, nread);
            while( (in+1) % N == out){
                pthread_cond_wait(&empty,&lock);//fa un lock a empty i s'en va a dormir
            };     // add item
            strcpy(buffer[in],buff);
            in = (in+1)%N;

            pthread_cond_signal(&full);//desperta un waiter de full si n'hi ha
            pthread_mutex_unlock(&lock);//fem unlock

        } else {
            printf("read_split EMPTY length %d\n", nread);
        }
    }
    printf("Thread READ Finished reading **********************\n");

    pthread_exit(0);
}

void* th_check(void *param)
{
    char buff[80];
    printf("Hello from a thread CHECK (consumer)\n");

    while(!readerFinished) {
        pthread_mutex_lock(&lock);//fem lock

        while(in==out){
            pthread_cond_wait(&full,&lock);//fa un lock a full i s'en va a dormir
        };
        strcpy(buff,buffer[out]);     // take item
        int temp = out;
        out = (out+1)%N;
        pthread_cond_signal(&empty);//desperta un waiter de empty si n'hi ha
        pthread_mutex_unlock(&lock);//fem unlock

        //ignore upper cases
        for(int l=0; l< strlen(buff);l++){
            buff[l] = tolower(buff[l]);
        }

        if (search_dict(&buff[0])) {
            printf("thread CHECK looking: %s CORRECT\n",buff);
        }else{
            printf("thread CHECK looking: %s INCORRECT\n",buff);
        }
    }
    pthread_exit(0);
}


int main(int argc, char *argv[])
{
    startTimer(TIMER);
    int i;

    if(argc > 1)  set_stdin_mode(); // Terminal Mode: characters read one by one

    pthread_mutex_init(&lock, NULL);//init lock
    pthread_cond_init(&full, 0);//init full
    pthread_cond_init(&empty, 0);//init empty

    load_dict(10);

    pthread_create(&threadsIds[0], NULL, th_read, NULL); 
    for(i=0;i<NUM_THREADS-1;i++) {
        pthread_create(&threadsIds[1+i], NULL, th_check, NULL); 
    }

    pthread_join(threadsIds[0], NULL);   // Waiting for the Reader
    readerFinished = true;

    printf("Master Thread Waiting for Checkers\n");   // Waiting for Checkers
    
    pthread_cond_broadcast(&full);//fem un broadcast de full, es a dir despertem a tots els consumidors
    
    for(i=1;i<NUM_THREADS;i++) {
        pthread_join(threadsIds[i], NULL);
    }
    printf("Master Thread Done\n");

    if(argc > 1) reset_stdin_mode();

    long time = endTimer(TIMER);
    printf("\nTime: %ld ms\n", time); //show total execution time

    return 0;
}

