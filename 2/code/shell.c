#include <string.h>
#include <stdio.h>
#include "myutils.h"
#include <stdbool.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_ARG_N 10
#define MAX_LEN 80

int main(){
	startTimer(1); //for showing the execution time

	char ch_end;
    	char buff[MAX_ARG_N][MAX_LEN];
	int num_chars = 1;
	int cmd_num = 1;
    
	while (num_chars > 0) {//mentres que num_chars sigui major que 0
	    	int index = 0;
		bool is_concurrent = false; //definim una variable boolean que la utilitzarem per determinar si es concurrent
        	ch_end = '0';

        while (ch_end != '\n' && num_chars > 0) {//mentre hi hagi paraules que llegir i no sigui la ultima d ela linia
           	 num_chars = read_split(0, &buff[index][0],MAX_LEN , &ch_end);//actualitzem num_chars amb l'enter que ens retorna read_split
            if (buff[index][0] == '&') {
                is_concurrent = true;//si hi ha '&' is_concurrent passa a ser true
                index--;
            }else if (num_chars != 0) {
                char *ampersand = strchr(&buff[index][0], '&');//retorna un punter a la primera ocurrencia a un '&', or NULL if the character is not found
                if (NULL != ampersand) {// si s'ha trobat el caracter '&'
                    is_concurrent = true; // si hi ha '&' is_concurrent passa a ser true
                    *ampersand = '\0';
                }
            }

            index++;
        }
        if(strlen(&buff[0][0])>0) {
            //create pointers for the execvp second argument
            char *pointers[MAX_ARG_N];
            for (int i = 0; i < MAX_ARG_N && i < index; i++) {
                pointers[i] = &buff[i][0];
                //printf("-C[%d] %s\n",i, &buff[i][0]);
            }
            pointers[index] = NULL;

            if (is_concurrent) {//si es concurrent
                printf("-- Command %d %s started running concurrently --\n", cmd_num, pointers[0]);
            } else {//si es sequencial
                printf("-- Command %d %s started running sequentially --\n", cmd_num, pointers[0]);
            }
            int pid = fork();//creem un fork
            if (pid == 0) {// child process
                execvp(buff[0], pointers);
                exit(0);
            }
            if (!is_concurrent) {//si es sequencial
                pid_t pid = wait(NULL);
            }

            cmd_num++;
        }
    }

    long time = endTimer(1);
    printf("\nTime: %ld ms\n", time); //show total execution time

    return 0;
}
