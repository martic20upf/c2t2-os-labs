#include <sys/types.h> /* pid_t */
#include <sys/wait.h>  /* waitpid */
#include <stdio.h>     /* printf, perror */
#include <stdlib.h>    /* exit */
#include <unistd.h>    /* _exit, fork */

int global_int = 33;

int main(void)
{
    pid_t pid;

    printf("Father speaking: my pid is %d.  I'm going to have children\n", getpid());

    pid = fork();
    
    if (pid == 0) global_int = 10; 
    if (pid > 0) global_int = 66; 

    if (pid == 0) {  
        printf("Hello from the child process: My pid is %d\n", getpid());
        sleep(1);
        printf("Hello from the child again: the value of global int is %d\n", global_int);
        exit(7);
    } 

    int status;

    int tok = wait(&status);

    printf("  -- %d  --  ", tok);

    printf("  -- %d  --  ", status);


    printf("  -- %d  --  ",  getpid());

    printf("Father speaking: Child Complete the value global_int: %d\n", global_int);

    // WIFEXITED,WEXITSTATUS Macro of the gnu lib POSIX standard
    if ( WIFEXITED(status) ) {   
        const int es = WEXITSTATUS(status);
        printf("Father speaking: Child Complete with exit status was %d\n", es);
    } else {
        printf("Father speaking: Child Complete without status\n");        
    }

    return 0;
}