#include <stdlib.h>

int** mat;

int** createMatrix( int n, int m) {
    int i;
    int** mat = malloc( n * sizeof(int*) );
    for(i=0;i<n;i++) {
        mat[i] = malloc( m * sizeof(int) );
    }
    return mat;
}

void freeMatrix(int** mat, int n, int m) {
    int i;
    free(mat);
}


int main() {
    int n = 5;
	mat = createMatrix(n,n);
	freeMatrix(mat,n,n);
	return 0;
}