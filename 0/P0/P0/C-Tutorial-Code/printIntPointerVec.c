#include <stdlib.h>
#include <stdio.h>

void printVector(int* v, int n) {
    int i;
    for(i=0;i<n;i++) printf("%d ",v[i]);
    printf("\n");
}

int main() {
	int i;
	int n = 5;
    int* vec = malloc(n*sizeof(int));
    for(i=0;i<n;i++) vec[i] = i;
    printVector(vec,5);         							
	return 0;
}