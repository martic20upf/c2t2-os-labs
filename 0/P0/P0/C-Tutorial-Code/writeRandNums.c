#include <unistd.h>     // unix-like system calls read and write
#include <fcntl.h>      // unix-like file handling : open
#include <stdio.h>      // printf, sprintf
#include <string.h>     // strlen()
#include <stdlib.h>     // rand()

int main(int argc, char* argv[]) {
    int i;
    float num;
    char buff[100];
    int fout = open( "nums.txt",  O_CREAT | O_RDWR);
    srand(13);
    for(i=0;i<10;i++) {
        num = rand()%1000 / 1000.0;
        sprintf(buff,"%f ",num);
        write(fout,buff,strlen(buff));
    }
    return 0;
}