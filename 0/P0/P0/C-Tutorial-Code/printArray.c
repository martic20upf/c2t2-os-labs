#include <stdlib.h>
#include <stdio.h>

void printVector(int* v, int n) {
    int i;
    for(i=0;i<n;i++) printf("%d ",v[i]);
    printf("\n");
}

int main() {
    int vec[5] = {1,2,3,4,5};   // an array vec[5] in C is different than an int pointer int*
    printVector(&vec[0],5);     // to acces the pointer we take the 1rst element vec[0] 
    							// and ask for its address &vec[0]
	return 0;
}