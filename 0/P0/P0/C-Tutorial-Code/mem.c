#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
	char* namep = "HELLO";
	char namearray[] = "HELLO";

	printf("namep %lu\n", sizeof(namep));
	printf("namearray %lu\n", sizeof(namearray));
 	printf("-------------\n\n\n");

 	char a[] = "Hello";
 	char* p = "Hello";
 	char* pa = &a[0];

 	char* pnew = malloc(10);
 	memcpy(pnew,p,5);

 	printf("-------------\n\n\n");

 	pa[0] = 'h';
 	pnew[0] = 'h';

 	printf("pa = %s    pnew = %s \n",pa, pnew);
 	printf("sizeof(a) = %ld    sizeof(p) = %ld\n",sizeof(a),sizeof(p));
 	printf("-------------\n\n\n");


	return 0;
}