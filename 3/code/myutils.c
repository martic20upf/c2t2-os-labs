#include "myutils.h"
#include <stdlib.h>
#include <termios.h>

struct timeval start[100];
struct termios saved_attributes;

void startTimer(int i) {
    if(i>=100) printf("Warning only 100 timers available!!");
    gettimeofday(&start[i], NULL);
}

long endTimer(int i) {
    long mtime, seconds, useconds;    
    struct timeval end;

    gettimeofday(&end, NULL);
    seconds  = end.tv_sec  - start[i].tv_sec;
    useconds = end.tv_usec - start[i].tv_usec;
    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    return mtime;
}



void reset_stdin_mode(void) {
  tcsetattr (STDIN_FILENO, TCSANOW, &saved_attributes);
}

void set_stdin_mode(void) {
  struct termios tattr;
  char *name;

  /* Make sure stdin is a terminal. */
  if (!isatty (STDIN_FILENO))
    {
      fprintf (stderr, "Not a terminal.\n");
      exit (EXIT_FAILURE);
    }

  /* Save the terminal attributes so we can restore them later. */
  tcgetattr (STDIN_FILENO, &saved_attributes);
  atexit (reset_stdin_mode);

  /* Set the funny terminal modes. */
  tcgetattr (STDIN_FILENO, &tattr);
  tattr.c_lflag &= ~(ICANON|ECHO); /* Clear ICANON and ECHO. */
  tattr.c_cc[VMIN] = 1;
  tattr.c_cc[VTIME] = 0;
  tcsetattr (STDIN_FILENO, TCSAFLUSH, &tattr);
}



int read_split( int fin, char* buff, int maxlen, char* ch_end ) {
    int i = 0;
    int oneread = 1;
    char c = '.';
    while(c != ' ' && c != '\n' && oneread == 1 && i < maxlen) {
        c = getchar();
        oneread = c != EOF;
        if(oneread) printf("read %c\n",c);
        if(c != ' ' && c != '\n' && oneread == 1) {
            buff[i] = c;
            i++;
        }
    }
    *ch_end = c;
    if(i < maxlen) buff[i] = '\0';
    return i;
}




void sem_init(semaphore* sem, int i) {
  sem->i = i;
  pthread_mutex_init(&sem->lock, NULL);
  pthread_cond_init(&sem->cond, NULL);
}


void sem_wait(semaphore* sem) {
   pthread_mutex_lock(&sem->lock);
   while(sem->i == 0) {
    pthread_cond_wait(&sem->cond, &sem->lock);
   }
   sem->i--;
   pthread_mutex_unlock(&sem->lock);
}


void sem_signal(semaphore* sem) {
   pthread_mutex_lock(&sem->lock);
   sem->i++;
   pthread_cond_signal(&sem->cond);
   pthread_mutex_unlock(&sem->lock);
}


