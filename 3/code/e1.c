#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>  // for sleep function : waits for seconds
#include <time.h>    // for usleep : waits for microseconds
#include <string.h>
#include <stdbool.h> 
#include "myutils.h"

int sum = 0;
// pthread_mutex_t lock;
semaphore sem;

void* fthread ( void* param ) {	
	for(int i=0;i<1000;i++){
		//pthread_mutex_lock(&lock);
		sem_wait(&sem);
		sum++;
		// pthread_mutex_unlock(&lock);
		sem_signal(&sem);
		usleep (1000);
	}
	return NULL ;
}

int main() {
	srand(time(0));

	int i = 0;
	startTimer(i);
	printf("Hello from master thread, this is a random number %d\n", rand()%10000);

	pthread_t fils[100];

	// pthread_mutex_init(&lock,NULL);
	sem_init(&sem, 1);

	for(int i=0;i<100;i++){
		pthread_create(&fils[i],NULL,fthread,NULL);
	}
	for(int i=0;i<100;i++){
		pthread_join(fils[i],NULL);
	}

	usleep(1000);
	printf("Main Thread: Timer %d ended with %ldms    sum = %d\n", i, endTimer(i), sum);
	
}