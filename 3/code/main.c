#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>  // for sleep function : waits for seconds
#include <time.h>    // for usleep : waits for microseconds
#include <string.h>
#include <stdbool.h> 

#include "myutils.h"

int sum = 0;

int main() {
	srand(time(0));

	int i = 0;
	startTimer(i);
	printf("Hello from master thread, this is a random number %d\n", rand()%10000);

	usleep(1000);
	printf("Main Thread: Timer %d ended with %ldms    sum = %d\n", i, endTimer(i), sum);
	return 0;
}